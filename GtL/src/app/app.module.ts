import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GridComponent } from './content/grid/grid.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { NavComponent } from './nav/nav.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { HeaderComponent } from './content/header/header.component';
import { MatMenuModule } from '@angular/material/menu';
import { LicenseDialogComponent } from './content/license-dialog/license-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatSnackBarModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './authentication/login/login.component';
import { LogoutComponent } from './authentication/logout/logout.component';
import { LicenseConfigComponent } from './content/license-config/license-config.component';
import { GridUserComponent } from './contentUser/grid-user/grid-user.component';
import { SupConfigComponent } from './contentUser/sup-config/sup-config.component';
import { DialogComponent } from './contentUser/dialog/dialog.component';
import { HeadUComponent } from './contentUser/head-u/head-u.component';
import {NgxPaginationModule} from 'ngx-pagination'; 
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AngularFontAwesomeModule } from 'angular-font-awesome';



@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    NavComponent,
    HeaderComponent,
    LicenseDialogComponent,
    LoginComponent,
    LogoutComponent,
    LicenseConfigComponent,
    GridUserComponent,
    SupConfigComponent,
    DialogComponent,
    HeadUComponent,
    
   
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatDialogModule,
    MatMenuModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AppRoutingModule,
    HttpClientModule,
    MatSnackBarModule,
    FormsModule,
    AngularFontAwesomeModule,
    NgxPaginationModule,
    PopoverModule.forRoot()

    

  ],
  providers: [
    MatDatepickerModule
  ],
  entryComponents: [
    LicenseDialogComponent,
    LicenseConfigComponent,
    DialogComponent,
    SupConfigComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
