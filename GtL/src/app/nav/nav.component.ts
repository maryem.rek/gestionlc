import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LicenceService } from '../service/licence/licence.service';
import * as moment from 'moment';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  list:any[];
  dely:number=3;

  constructor(private breakpointObserver: BreakpointObserver,private licenceservice:LicenceService) {
    this.licenceservice.getData.subscribe(data=>{ 
      this.list =data;  
    })
  }

  getDate(date:any){  
  return ( moment(date).diff(moment(),'days')>=0  && moment(date).diff(moment(),'days')<=this.dely );
  }

  getCount(){
  return this.list.filter(e=>( moment(e.dateEx).diff(moment(),'days')>=0  && moment(e.dateEx).diff(moment(),'days')<=this.dely )).length;
  }
}
