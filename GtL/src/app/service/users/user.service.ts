import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/users/user';
const headr={
  headers:new HttpHeaders({
    'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private api: string = environment.api;
  
  constructor(private http: HttpClient) { }

  public getUser(): Observable<User[]> {
    return this.http.get<User[]>(this.api+'users');
  }

  
  AddUser(user: any): Observable<any> {
    return this.http.post(this.api+'users',user ,headr);
  }

  deleteUser(id: number): Observable<any> {  
    return this.http.delete(this.api+'users/'+id,headr);  
  } 

  updateUser( user: User): Observable<any>{  
    
    return this.http.put(this.api+'users', user);
  } 
  

}
