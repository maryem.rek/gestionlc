import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { License } from 'src/app/models/licence/license';
const headr={
  headers:new HttpHeaders({
    'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn:'root'
})
export class LicenceService {

  private api: string = environment.api;

private data = new BehaviorSubject(<any>[]);
getData =  this.data.asObservable();

  constructor(private http: HttpClient) {
   }

   public getLicences(): Observable<License[]> {
    return this.http.get<License[]>(this.api+'licences');
  }

  AddLicence(license: any): Observable<any> {
    return this.http.post(this.api+'licence',license ,headr);
  }


  deleteLicence(id: number): Observable<any> {
    return this.http.delete(this.api+'licence/'+id,headr);
  }

  updateLicence( license: License): Observable<any>{
    console.log("licence ",license);
    return this.http.put(this.api+'licence', license);
  }

  notifyData(data){
    this.data.next(data);
  }
  

}
