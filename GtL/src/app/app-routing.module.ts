import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GridComponent } from './content/grid/grid.component';
import { GridUserComponent } from './contentUser/grid-user/grid-user.component';


const routes: Routes = [
  { path: '', redirectTo: 'licence', pathMatch: 'full' },
  { path: 'licence', component: GridComponent },
  { path: 'util', component: GridUserComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
