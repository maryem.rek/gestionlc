import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from 'src/app/service/users/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-sup-config',
  templateUrl: './sup-config.component.html',
  styleUrls: ['./sup-config.component.css']
})
export class SupConfigComponent implements OnInit {

  constructor(
    private userService: UserService,
    public dialogRef: MatDialogRef<SupConfigComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data:any
  ) { }

  ngOnInit() {
  }

  remove(){
    this.userService.deleteUser(this.data.id)
    .subscribe(
      data => {
        this.cancel();
  })
}

cancel() {
  this.dialogRef.close();
}
}
