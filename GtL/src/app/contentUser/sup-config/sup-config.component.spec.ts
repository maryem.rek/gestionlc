import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupConfigComponent } from './sup-config.component';

describe('SupConfigComponent', () => {
  let component: SupConfigComponent;
  let fixture: ComponentFixture<SupConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
