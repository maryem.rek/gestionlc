import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { User } from 'src/app/models/users/user';
import { UserService } from 'src/app/service/users/user.service';
import { MatDialog } from '@angular/material';
import { EventEmitter } from 'events';
import { SupConfigComponent } from '../sup-config/sup-config.component';
import { DialogComponent } from '../dialog/dialog.component';


@Component({
  selector: 'app-grid-user',
  templateUrl: './grid-user.component.html',
  styleUrls: ['./grid-user.component.css']
})
export class GridUserComponent implements OnInit {
  
  @Output() refresh= new EventEmitter();
  users: User[]= [];
  firstname: String;
  page: number = 1;
 
  constructor(private userService: UserService, public dialog: MatDialog) { }

  ngOnInit() {
  
    this.userService.getUser().subscribe(data =>{
      this.users=data;
    })
  }

  getData(){
    this.ngOnInit();
  }

  remove(user:User) { 
    const dialogRef = this.dialog.open( SupConfigComponent , {
      width: '450px',
      data: {id:user.id}
    });

    dialogRef.afterClosed().subscribe(result => {
    this.ngOnInit();
    });
  }

  update(user:User){
    console.log('update',user);
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: {
        data:user
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
      });
  }

// pour la recherche
  Search(){
    if(this.firstname !=""){
    this.users=this.users.filter(res=>{
    return res.nom.toLocaleLowerCase().match(this.firstname.toLocaleLowerCase())
    });
    }else if(this.firstname == ""){
    this.ngOnInit();
    }
    }
}
