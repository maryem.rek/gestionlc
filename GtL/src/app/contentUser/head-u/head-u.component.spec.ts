import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadUComponent } from './head-u.component';

describe('HeadUComponent', () => {
  let component: HeadUComponent;
  let fixture: ComponentFixture<HeadUComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadUComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadUComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
