import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-head-u',
  templateUrl: './head-u.component.html',
  styleUrls: ['./head-u.component.css']
})
export class HeadUComponent implements OnInit {
  @Output() refresh= new EventEmitter();
 
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  open() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh.emit('rrr');
    });
  }


}
