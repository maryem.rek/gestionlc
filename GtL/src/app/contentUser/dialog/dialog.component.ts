import { Component, OnInit, Inject, OnChanges } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { User } from 'src/app/models/users/user';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from 'src/app/service/users/user.service'

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit, OnChanges {

  UserForm: FormGroup;
  isUpdate:Boolean=false;
  user: User;
  
  constructor(
    private userService: UserService,
    private alert:MatSnackBar,
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit() {

  this.isUpdate = (this.data && this.data.data);
    this.UserForm = new FormGroup({
      id: new FormControl(''),
      nom: new FormControl(''),
      ste: new FormControl(''),
      email: new FormControl(''),
      role: new FormControl(''),
      
      
    });
    if(this.data){
      this.UserForm.setValue({
        id: this.data.data.id,
        nom: this.data.data.nom,
        ste: this.data.data.ste,
        email: this.data.data.email,
        role: this.data.data.role
        
      }
      );
    }
  }

  ngOnChanges(){ 
  }

  cancel() {
    this.dialogRef.close();
  }
  
  onSubmit() {
    if(this.isUpdate){
      // console.log('vrai update ', this.licenseForm.value)
      this.userService.updateUser(this.UserForm.value)
    .subscribe(
      dataUpdated => {
        this.ngOnChanges();
        this.cancel();
  })
    }else{
      this.create(this.UserForm.value);
    }
  
    // console.log(this.UserForm.value);
  }
  create(user:User){
    this.userService.AddUser(user).subscribe(data=>{
      this.cancel();
      this.alert.open('Votre user à été bien ajouté !');
      })
  }

  
  update(user: User){
    this.userService.updateUser(this.UserForm.value)
    .subscribe(
      data => {
        this.cancel();
        this.alert.open('Votre User à été bien modifié !');
  })
  }

}
