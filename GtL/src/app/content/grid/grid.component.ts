import {Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LicenceService } from 'src/app/service/licence/licence.service';
import { LicenseDialogComponent } from '../license-dialog/license-dialog.component';
import { LicenseConfigComponent } from '../license-config/license-config.component';
import { License } from 'src/app/models/licence/license';


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
  page: number = 1;
  licenses: License[]= [];
  private firstname:string;
 
  constructor(private licenceService: LicenceService,public dialog: MatDialog) {
  }

  ngOnInit() {
    this.licenceService.getLicences().subscribe(data => {
      console.log('data',data);
      this.licenses= data;
      this.licenceService.notifyData(data);
    })
  }

  remove(licence:License) {
    const dialogRef = this.dialog.open( LicenseConfigComponent, {
      width: '450px',
      data: {id:licence.id}
    });

    dialogRef.afterClosed().subscribe(result => {
    this.ngOnInit();
    });
  }

  update(licence:License){
    const dialogRef = this.dialog.open(LicenseDialogComponent, {
      width: '350px',
      data: {
        data:licence
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
      });
  }

  getData(){
    this.ngOnInit();
  }

  // pour la recherche
  Search(){
    if(this.firstname !=""){
    this.licenses=this.licenses.filter(res=>{
    return res.nom.toLocaleLowerCase().match(this.firstname.toLocaleLowerCase())
    });
    }else if(this.firstname == ""){
    this.ngOnInit();
    }
    }
}
