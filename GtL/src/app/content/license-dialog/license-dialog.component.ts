import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LicenceService } from 'src/app/service/licence/licence.service';
import { MatSnackBar } from '@angular/material';
import { License } from 'src/app/models/licence/license';

@Component({
  selector: 'app-license-dialog',
  templateUrl: './license-dialog.component.html',
  styleUrls: ['./license-dialog.component.css']
})
export class LicenseDialogComponent implements OnInit,OnChanges {
  licenseForm: FormGroup;
  isUpdate:Boolean=false;
  licence: License;
  constructor(
    private licenceService: LicenceService,
    private alert:MatSnackBar,
    public dialogRef: MatDialogRef<LicenseDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit() {
    console.log('up',this.data);
    this.isUpdate = (this.data && this.data.data);
    this.licenseForm = new FormGroup({
      id: new FormControl(''),
      nom: new FormControl(''),
      nomClient: new FormControl(''),
      dateA: new FormControl(''),
      dateEx: new FormControl(''),
    });

    if(this.data){
      this.licenseForm.setValue({
        id: this.data.data.id,
        nom: this.data.data.nom,
        nomClient: this.data.data.nomClient,
        dateA: new Date(this.data.data.dateA),
        dateEx: new Date(this.data.data.dateEx)
      });
    }
  }

  ngOnChanges(){}

  cancel() {
    this.dialogRef.close();
  }
  
  onSubmit() {
    if(this.isUpdate){
      console.log('vrai update ', this.licenseForm.value)
      this.licenceService.updateLicence(this.licenseForm.value)
    .subscribe(
      dataUpdated => {
        this.ngOnChanges();
        this.cancel();
  })
    }else{
      this.create(this.licenseForm.value);
    }

  }
  create(licence:License){
    this.licenceService.AddLicence(licence).subscribe(data=>{
      this.cancel();
      this.alert.open('Votre Licence à été bien ajouté !');
      })
  }

  
  update(license: License){
    this.licenceService.updateLicence(this.licenseForm.value)
    .subscribe(
      data => {
        this.cancel();
        this.alert.open('Votre Licence à été bien modifié !');
  })
  }
}
