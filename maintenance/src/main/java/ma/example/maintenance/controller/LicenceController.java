package ma.example.maintenance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ma.example.maintenance.dto.LicenceDto;
import ma.example.maintenance.service.LicenceService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/")
public class LicenceController {
    @Autowired
    LicenceService licenceService;

    @GetMapping("licence/{id}")
    public LicenceDto find(@PathVariable Long id){
        return licenceService.find(id);
    }

    @PostMapping("/licence")
    public void add(@RequestBody LicenceDto licence){

        licenceService.add(licence);
    }

    @PutMapping("/licence")
    public void edit(@RequestBody LicenceDto licence) {
        licenceService.edit(licence);
    }

    @DeleteMapping("licence/{id}")
    public String edit(@PathVariable("id") Long id){
       return licenceService.delete(id);
    }

    @GetMapping("licences")
    public List<LicenceDto> getAll(){
        return licenceService.getAll();
    }

    @GetMapping("licence/{nom}")
    public List<LicenceDto> find(@PathVariable String nom) {
        return licenceService.search(nom);
    }
}
