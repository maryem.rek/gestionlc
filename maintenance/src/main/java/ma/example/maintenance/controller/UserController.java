package ma.example.maintenance.controller;

import ma.example.maintenance.domaine.User;
import ma.example.maintenance.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("users")
    public ResponseEntity<?> add(@RequestBody User user){
        return userService.add(user);
    }


    @PutMapping("users")
    public User edit(@RequestBody User user){
        return userService.edit(user);
    }

    @DeleteMapping("users/{id}")
    public void edit(@PathVariable("id") Long id){
        userService.delete(id);
    }

    @GetMapping("users")
    public List<User> getAll(){
        return userService.getAll();
    }


}
