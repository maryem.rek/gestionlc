package ma.example.maintenance.service;

import ma.example.maintenance.domaine.User;
import ma.example.maintenance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<User> getAll(){
        return userRepository.findAll();
    }

    public User find(Long id){
        Optional<User> users = userRepository.findById(id);
        if(users.isPresent()){
            return users.get();
        }
        return null;
    }

    public ResponseEntity<?> add(User u){

        if(u.getId()!=null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(userRepository.save(u),HttpStatus.OK);
    }
    public User edit(User u){
        if(u.getId()==null || find(u.getId())==null)
            return u;
        return userRepository.save(u);
    }

    public void delete(Long id){
        Optional<User> user =  userRepository.findById(id);
        if(user.isPresent())
            userRepository.delete(user.get());
    }


}
