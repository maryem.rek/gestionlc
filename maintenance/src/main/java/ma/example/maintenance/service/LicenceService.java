package ma.example.maintenance.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.example.maintenance.dto.LicenceDto;
import ma.example.maintenance.domaine.Licence;
import ma.example.maintenance.mapper.EntityMapper;
import ma.example.maintenance.repository.LicenceRepository;

@Service
public class LicenceService {

	@Autowired
	LicenceRepository licenceRepository;

	public LicenceDto find(Long id) {
		Optional<Licence> licences = licenceRepository.findById(id);
		if (licences.isPresent()) {
			LicenceDto licenceDto=EntityMapper.convertToDto(licences.get());
					licenceDto.setStatus(isExpired(licences.get()));
			return licenceDto;
		}
		return null;
	}

	public Licence add(LicenceDto licence) {

		return licenceRepository.save(EntityMapper.convertToEntity(licence));
	}

	public Licence edit(LicenceDto licence) {

		return licenceRepository.save(EntityMapper.convertToEntity(licence));
	}

	public String delete(Long id) {
		Licence licence = EntityMapper.convertToEntity(find(id));
		if(licence != null)
			licenceRepository.deleteById(id);
		 // licenceRepository.delete(licence);

		return "";

	}

	public List<LicenceDto> getAll() {
		List<Licence> listLicence = licenceRepository.findAll();
		List<LicenceDto> listLicenceDto = new ArrayList<>();

		 listLicence.stream().forEach(licence -> {

			LicenceDto dto = EntityMapper.convertToDto(licence);
			if (licence.getDateA().before(licence.getDateEx())) {
				dto.setStatus(isExpired(licence));
			}
			listLicenceDto.add(dto);
		});
		return listLicenceDto;
	}

	public List<LicenceDto> search(String nom) {
		List<Licence> listLicence=licenceRepository.findByNom(nom);
		List<LicenceDto> listLicenceDto = new ArrayList<>();
		if(null!=licenceRepository.findByNom(nom)) {
			listLicence.stream().forEach(licence->{
				listLicenceDto.add(new LicenceDto(licence.getId(), licence.getnom(), licence.getnom(), licence.getDateA(), licence.getDateEx(),isExpired(licence)));
			}
			);
		}
		return listLicenceDto;
	}
	private boolean isExpired(Licence licence) {
		long millis=System.currentTimeMillis();
		Date now=new Date(millis);

		if(licence.getDateEx().after(now)) {
			return true;
		}else {
			return false;
		}
	}
}
