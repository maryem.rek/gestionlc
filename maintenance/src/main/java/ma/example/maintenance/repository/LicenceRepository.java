package ma.example.maintenance.repository;

import ma.example.maintenance.domaine.Licence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LicenceRepository extends JpaRepository<Licence, Long> {


    @Override
    List<Licence> findAll();

    Optional<Licence> findById(Long id);
    List<Licence> findByNom(String nom);
}
