package ma.example.maintenance.dto;

import java.util.Date;

public class LicenceDto {
	private Long id;
    private String nom;
    private String nomClient;
    private Date dateA;
    private Date dateEx;
    private boolean status;



	public LicenceDto(Long id, String nom, String nomClient, Date dateA, Date dateEx) {
		super();

		this.id = id;
		this.nom = nom;
		this.nomClient = nomClient;
		this.dateA = dateA;
		this.dateEx = dateEx;
	}

    
	public LicenceDto(Long id, String nom, String nomClient, Date dateA, Date dateEx, boolean status) {
		super();
		this.id = id;
		this.nom = nom;
		this.nomClient = nomClient;
		this.dateA = dateA;
		this.dateEx = dateEx;
		this.status = status;
	}


	public LicenceDto() {
    	
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public final String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public final void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the nomClient
	 */
	public final String getNomClient() {
		return nomClient;
	}
	/**
	 * @param nomClient the nomClient to set
	 */
	public final void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}
	/**
	 * @return the dateA
	 */
	public final Date getDateA() {
		return dateA;
	}
	/**
	 * @param dateA the dateA to set
	 */
	public final void setDateA(Date dateA) {
		this.dateA = dateA;
	}
	/**
	 * @return the dateEx
	 */
	public final Date getDateEx() {
		return dateEx;
	}
	/**
	 * @param dateEx the dateEx to set
	 */
	public final void setDateEx(Date dateEx) {
		this.dateEx = dateEx;
	}
	/**
	 * @return the status
	 */
	public final boolean isStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public final void setStatus(boolean status) {
		this.status = status;
	}
    

}
