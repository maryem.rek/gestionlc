package ma.example.maintenance.mapper;

import ma.example.maintenance.dto.LicenceDto;
import ma.example.maintenance.domaine.Licence;

public class EntityMapper {
	
	public static Licence convertToEntity(LicenceDto licenceDto) {
		return new Licence(licenceDto.getId(),licenceDto.getNom(), licenceDto.getNomClient(), licenceDto.getDateA(), licenceDto.getDateEx());
		
	}
	public static LicenceDto convertToDto(Licence licence) {
		return new LicenceDto(licence.getId(), licence.getnom(),licence.getnomClient() , licence.getDateA(), licence.getDateEx());
		
	}

}
