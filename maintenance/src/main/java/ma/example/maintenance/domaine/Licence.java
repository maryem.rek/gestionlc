package ma.example.maintenance.domaine;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "licence")
public class Licence {
    @Id
    @GeneratedValue
    private Long id;

    private String nom;
    @Column(name="nom_client")
    private String nomClient;
    @Column(name="dataea")
    private Date dateA;
    @Column(name="date_ex")
    private Date dateEx;

    public Licence(String nom, String nomClient, Date dateA, Date dateEx) {
        this.nom = nom;
        this.nomClient = nomClient;
        this.dateA = dateA;
        this.dateEx = dateEx;
    }
    public Licence( Long id,String nom, String nomClient, Date dateA, Date dateEx) {
        this.id = id;
        this.nom = nom;
        this.nomClient = nomClient;
        this.dateA = dateA;
        this.dateEx = dateEx;
    }

    public Licence() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getnom() {
        return nom;
    }

    public void setnom(String nom) {
        this.nom = nom;
    }

    public String getnomClient() {
        return nomClient;
    }

    public void setnomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public Date getDateA() {
        return dateA;
    }

    public void setDateA(Date dateA) {
        this.dateA = dateA;
    }

    public Date getDateEx() {
        return dateEx;
    }

    public void setDateEx(Date dateEx) {
        this.dateEx = dateEx;
    }
}
